/*! \file  loadWPM.c
 *
 *  \brief Calculate new time constants based on wpm value
 *
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:45 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "Keyer01.h"

/*! loadWPM - Calculate new time constants based on wpm value */
/*!
 *
 */
void loadWPM (int wpm)
{
    ulDitTime = 1200/wpm;
}
