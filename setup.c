/*! \file  setup.c
 *
 *  \brief System Initialization
 *
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:49 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "Keyer01.h"
#include <stdio.h>
#include "../include/LCD.h"

extern unsigned long ulMilliseconds;

/*! setup - Initialization for Keyer01 */
/*! setup() sets the pin direction for used pins and initializes
 *  a few variables.
 */
void setup() 
{
  char szWork[32];
  
    // Set clock to 70 MIPS
  CLKDIVbits.FRCDIV = 0; // Divide by 1 = 8MHz (default /2)
  CLKDIVbits.PLLPRE = 0; // Divide by 2 = 4 MHz (default)
  PLLFBD = 72; // Multiply by 70 = 280 (default *50)
  CLKDIVbits.PLLPOST = 0; // Divide by 2 = 140 = 70 MIPS (def. 50/25)

  /* Initialize timer for millis() */
  ulMilliseconds = 0;
  T2CONbits.TCKPS = 1;
  //PR2 = 8484;
  PR2 = 8670;
  T2CONbits.TON = 1;
  _T2IF = 0;
  _T2IE = 1;
  
  /* Initialize PWM for tone */
  
  // Use Timer 5 for PWM
  PR5 = 30000;
  T5CONbits.TCKPS = 2;
  T5CONbits.TON = 1;

  // Set up OC2 for speaker
  OC2R = 0;
  OC2RS = 1;
  OC2CON1bits.OCSIDL = 1;
  OC2CON1bits.OCTSEL = 7; // Peripheral clock
  OC2CON1bits.OCTSEL = 3; // T5 clock
  OC2CON1bits.OCM = 5; // Edge-aligned PWM
  OC2CON2bits.SYNCSEL = 15;
  PR5 = (unsigned int) (530419.1667/600.0);
  OC2RS = PR5>>1;
  
 
    // Setup outputs
    PIN_LED_TRIS = 0;       /* LED pin output */
 
    // Setup control input pins
    _ANSA0 = 0;             /* A0 and A1 pins used as digital */
    _ANSA1 = 0;
    PIN_L_TRIS = 1;         /* A0 and A1 pins inputs (redundant) */
    PIN_R_TRIS = 1;

    PIN_LED = 0;            /* Turn off the LED */
 
    ucKeyerState = IDLE;
    ucKeyerControl = IAMBICB; /* Or 0 for IAMBICA */
//    ucKeyerControl = 0; /* Or 0 for IAMBICA */
    
    fWeight = 3.4;
    nCodeSpeed = 20;
    loadWPM(nCodeSpeed);            /* Fix speed at 23 WPM (for now) */
    
    LCDinit();
    LCDclear();
    
    sprintf(szWork,"1.0  %s",__DATE__);
    LCDline2();
    LCDputs(szWork);
    sprintf(szWork,"Keyer   %s",__TIME__);
    LCDhome();
    LCDputs(szWork);
    nDisplayDirty = 1;
}
