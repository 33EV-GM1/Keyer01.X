/*! \file  Keyer01.c
 *
 *  \brief Mainline for PIC version of the iambic keyer
 * 
 * Basic Iambic Keyer
 * ucKeyerControl contains processing flags and keyer mode bits
 * Supports Iambic A and B
 * State machine based, uses calls to millis() for timing.
 *
 *  Based on the work of Steven T. Elliott with cleanup by
 *  Bill Bishop.
 * 
 * https://www.wrbishop.com/ham/arduino-iambic-keyer-and-side-tone-generator/
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:37 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 by Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details:
 *
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 *  Boston, MA  02111-1307  USA
 *
 *  http://openqrp.org/?p=343
 *
 */
#include <xc.h>
#include "configBits.h"
#define EXTERN
#include "Keyer01.h"

/*! main - mainline for Keyer01 */
/*! main() is pretty much a dummy to mimic the Arduino environment. All
 *  it does is calls setup() and then endlessly calls loop().
 */
int main( void )
{
  setup();
  
  while(1) 
    loop();
  
  return 0;
}
