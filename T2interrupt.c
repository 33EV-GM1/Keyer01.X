/*! \file  T2interrupt.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 6, 2016, 7:08 AM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "Keyer01.h"

unsigned long ulMilliseconds;

/*! T2interrupt - */

/*!
 *
 */
void __attribute__((__interrupt__, auto_psv)) _T2Interrupt( void)
{
  _T2IF = 0; // Clear the interrupt flag
  ulMilliseconds++;
}