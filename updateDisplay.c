/*! \file  updateDisplay.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 16, 2016, 5:29 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include "../include/LCD.h"
#include "Keyer01.h"

/*! updateDisplay - */

/*!
 *
 */
void updateDisplay(void)
{
  char szWork[32];
  LCDclear();
  sprintf(szWork,"Spd=%2d, Wgt=%4.1f     ",nCodeSpeed,fWeight);
  LCDline2();
  LCDputs(szWork);
  LCDhome();  
  nDisplayDirty = 0;
}
