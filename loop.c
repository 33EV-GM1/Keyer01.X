/*! \file  loop.c
 *
 *  \brief Main Work Loop
 *
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:54 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdio.h>
#include "Keyer01.h"
#include "../include/LCD.h"

extern unsigned long ulMilliseconds;

/*! loop - Main program loop*/
/*! loop() is the main program loop for Keyer01.  Mainline calls it
 *  endlessly following setup().
 */
void loop()
{

  switch (ucKeyerState) {
    case IDLE:
        // Wait for direct or latched paddle press
      if ( (PIN_L == LOW) ||
              (PIN_R == LOW) ||
              ( ucKeyerControl & 0x03 ))
        {
            update_PaddleLatch();
            ucKeyerState = CHK_DIT;
            if ( nDisplayDirty )
              {
                updateDisplay();
              }
        }
        break;
 
    case CHK_DIT:
        // See if the dit paddle was pressed
        if (ucKeyerControl & DIT_L) {
            ucKeyerControl |= DIT_PROC;
            ulElementTimer = ulDitTime;
            ucKeyerState = KEYED_PREP;
        }
        else {
            ucKeyerState = CHK_DAH;
        }
        break;
 
    case CHK_DAH:
        // See if dah paddle was pressed
        if (ucKeyerControl & DAH_L) {
            ulElementTimer = ulDitTime*3;
            ulElementTimer = (unsigned long)( (double)ulDitTime * fWeight );
            ucKeyerState = KEYED_PREP;
        }
        else {
            ucKeyerState = IDLE;
        }
        break;
 
    case KEYED_PREP:
        // Assert key down, start timing, state shared for dit or dah
        PIN_LED = 1;
        tone( 1 );
        ulElementTimer += millis();                 // set ulElementTimer to interval end time
        ucKeyerControl &= ~(DIT_L + DAH_L);   // clear both paddle latch bits
        ucKeyerState = KEYED;                 // next state
        break;
 
    case KEYED:
        // Wait for timer to expire
        if (millis() > ulElementTimer) {            // are we at end of key down ?
            PIN_LED = 0;
            tone( 0 );
            ulElementTimer = millis() + ulDitTime;    // inter-element time
            ucKeyerState = INTER_ELEMENT;     // next state
        }
        else if (ucKeyerControl & IAMBICB) {
            update_PaddleLatch();           // early paddle latch in Iambic B mode
        }
        break;
 
    case INTER_ELEMENT:
        // Insert time between dits/dahs
        update_PaddleLatch();               // latch paddle state
        if (millis() > ulElementTimer) {            // are we at end of inter-space ?
            if (ucKeyerControl & DIT_PROC) {             // was it a dit or dah ?
                ucKeyerControl &= ~(DIT_L + DIT_PROC);   // clear two bits
                ucKeyerState = CHK_DAH;                  // dit done, check for dah
            }
            else {
                ucKeyerControl &= ~(DAH_L);              // clear dah latch
                ucKeyerState = IDLE;                     // go idle
            }
        }
        break;
    }
}
