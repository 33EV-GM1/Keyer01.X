/*! \file  Keyer01.h
 *
 *  \brief Function prototypes, globals and manifest constants
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:37 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef KEYER01_H
#define	KEYER01_H

#include <xc.h>

#ifdef	__cplusplus
extern "C"
{
#endif

//
// Basic definitions
//
#define     LOW             0
#define     HIGH            1
//
// Digital Pins
//
#define     PIN_LED         _LATB4
#define     PIN_LED_TRIS    _TRISB4
#define     PIN_L           _RA0
#define     PIN_L_TRIS      _TRISA0
#define     PIN_R           _RA1
#define     PIN_R_TRIS      _TRISA1
#define     PIN_TONE        _LATB9
#define     PIN_TONE_TRIS   _TRISB9
//
//  ucKeyerControl bit definitions
//
#define     DIT_L      0x01     // Dit latch
#define     DAH_L      0x02     // Dah latch
#define     DIT_PROC   0x04     // Dit is being processed
#define     PDLSWAP    0x08     // 0 for normal, 1 for swap
#define     IAMBICB    0x10     // 0 for Iambic A, 1 for Iambic B

// USed (eventually) in tone()
#define NOTE_D5  587    // "pitch.h" at http://arduino.cc/en/Tutorial/Tone
 
///////////////////////////////////////////////////////////////////////////////
//
//  State Machine Defines
 
enum KSTYPE {IDLE, CHK_DIT, CHK_DAH, KEYED_PREP, KEYED, INTER_ELEMENT };

void setup(void);
void loop(void);
void loadWPM (int);
void update_PaddleLatch(void);
unsigned long millis(void);
void tone( int );
void updateDisplay(void);

#ifndef EXTERN
#define EXTERN extern
#endif

//
//  Global Variables
//
EXTERN unsigned long       ulDitTime;                    // No. milliseconds per dit
EXTERN unsigned char       ucKeyerControl;
EXTERN unsigned char       ucKeyerState;
EXTERN unsigned long       ulElementTimer;
EXTERN int nDisplayDirty;
EXTERN int nCodeSpeed;
EXTERN double fWeight;

#ifdef	__cplusplus
}
#endif

#endif	/* KEYER01_H */

