/*! \file  millis.c
 *
 *  \brief Dummy routine to count milliseconds
 *
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:43 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "Keyer01.h"

extern unsigned long ulMilliseconds;

/*! millis - Count milliseconds */
/*!
 *
 */
unsigned long millis()
{
  return ulMilliseconds;
}

