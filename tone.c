/*! \file  tone.c
 *
 *  \brief Turn tone on and off
 *
 *
 *  \author jjmcd
 *  \date July 7, 2016, 4:28 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

#define RB9PPS          RPOR3bits.RP41R     //!< Mapping register for RB9
#define MAPOC2          17                  //!< Map Output Compare 2

/*! tone - */
/*!
 *
 */
void tone(int onoff)
{
  if (onoff)
    RB9PPS = MAPOC2;
  else
    RB9PPS = 0;
}
