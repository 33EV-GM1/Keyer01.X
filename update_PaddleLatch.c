/*! \file  update_PaddleLatch.c
 *
 *  \brief Latch dit and/or dah press
 *
 *
 *  \author jjmcd
 *  \date July 5, 2016, 8:47 AM
 *
 * Software License Agreement
 * Copyright (c) 2009 Steven T. Elliott
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "Keyer01.h"

/*! update_PaddleLatch - Latch dit and/or dah press */
/*!
 * Called by keyer routine
 */
void update_PaddleLatch()
{
  if ( PIN_R == LOW )
    {
        ucKeyerControl |= DIT_L;
    }
  if ( PIN_L == LOW )
    {
        ucKeyerControl |= DAH_L;
    }
}
 
